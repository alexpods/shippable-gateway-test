FROM nginx:1.11
MAINTAINER Alexey Podskrebyshev <alexey.podskrebyshev@gmail.com>
WORKDIR /etc/nginx

ENV \
  TEACHME_WEB_HOST='localhost' \
  TEACHME_WEB_PORT='8001' \
  TEACHME_SERVER_HOST='localhost' \
  TEACHME_SERVER_PORT='8002'

COPY nginx.conf ./nginx.conf.tmpl
COPY boot.sh ./boot.sh

# COPY cert.pem /etc/ssl/certs/localhost.pem
# COPY key.pem /etc/ssl/private/localhost.pem

CMD ["bash", "boot.sh"]