envsubst '
  $TEACHME_WEB_HOST
  $TEACHME_WEB_PORT
  $TEACHME_SERVER_HOST
  $TEACHME_SERVER_PORT
' < /etc/nginx/nginx.conf.tmpl > /etc/nginx/nginx.conf

nginx -g 'daemon off;'
